# L'officiel de la saison 5 de HolyCube

Voici le datapack qui est utilisé sur la saison 5 de HolyCube.
La liste des fonctionnalités incluses:
* Nouvelles recettes;
* Un module pour détecter les joueurs AFK;
* La gestion d'un pourcentage de joueurs pour dormir;
* Un objet craftable pour casser une zone de bedrock de 3x3x5 sur le toit du nether;
* Affichage de l'heure par une bossbar lorsque le joueur a une horloge dans son inventaire;
* Un module pour gérer automatiquement le loot de têtes de joueurs lors de leurs morts;
* Un casque en netherite qui permet d'avoir vision nocturne dans le nether. Disponible en vente chez les Wandering Traders de façon aléatoire;
* Vente de têtes de joueurs aléatoires chez les Wandering Traders;
* Vente de miniblocs sous format de lootboxes chez les Wandering Traders;
* Drop d'oeufs de dragon, double drop de shulker shells et anti-grief d'enderman.

## Installation
Tous les datapacks s'installent dans le dossier `world/datapacks/` d'une sauvegarde MineCraft. Cet article du wiki gamepedia de MineCraft vous aidera plus en profondeur: [Tutoriels/Installer un pack de données](https://minecraft-fr.gamepedia.com/Tutoriels/Installer_un_pack_de_donn%C3%A9es).

- Placez les fichiers `.zip` venant de l'onglet [Releases](https://gitlab.com/holycube/datapack/-/releases) dans votre dossier datapacks dans votre monde MineCraft.

## Configuration
### Activer/Désactiver un module
  `/data modify storage holycube:config enabled.<nom> set value <0/1>b` Active `1b` ou désactive `0b` le module.
  `afk bed bedrock clock head helmet loot_head miniblock trader`
### AFK
  `/data modify storage holycube:config afk_timer set value <int>` Modifie la valeur de temps `<int>` qu'il faut pour être considéré comme AFK.
  L'unité est basée en secondes.
### BED
  `/data modify storage holycube:config bed_req set value <int>` Modifie le pourcentage nécessaire pour passer la nuit. **Doit être entre 1 et 100!**

## Crafts personnalisés
### Bedrock Breaker  
  `P N P` `P -> Diamond Pickaxe`  
  `N # N` `N -> End Crystal`  
  `P N P` `# -> Netherite Ingot`

### Laines colorées
  `L L L` `L -> Laine blanche`  
  `L # L` `# -> Colorant`  
  `L L L`  

### Red Nether Bricks
  `N W` `N -> Nether Bricks`  
  `W N` `W -> Nether Wart Block`

### Quartz Block
  `D D` `D -> Diorite`  
  `Sans forme précise.`

### Dalles et Escaliers
  `Mettre n'importe quel bloc de planche pour obtenir 2 dalles ou 1 escalier du même bois.`

### Trapdoor
  `Le nombre de trapdoor craftées à été augmenté à 6 au lieu de 4.`
